//
//  TOUInfoViewController.swift
//  ThinkOfU
//
//  Created by Larissa Kalbhenn on 05.07.15.
//  Copyright (c) 2015 com.tailored-apps. All rights reserved.
//

import Foundation
import UIKit


class TOUInfoViewController: UIViewController {
    
    var smallLabel: UILabel!
    var infoTextLabel : UILabel!
    var universityLabel : UILabel!
    var lvaLabel : UILabel!
    
    var bgColor_ENABLED : UIColor!
    var bgColor_DISABLED : UIColor!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bgColor_ENABLED = UIColor.fromHex(0x87CFD8)
        bgColor_DISABLED = UIColor.fromHexAlpha(0x87CFD8)
        
        view.backgroundColor = UIColor.whiteColor()
        navigationController?.navigationBar.hidden = false
        navigationController?.navigationBar.translucent = true
        navigationController?.navigationBar.tintColor = bgColor_ENABLED
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "OK", style: .Plain, target: self, action: "okAction")

        let navHight = navigationController?.navigationBar.frame.size.height
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 15
        paragraphStyle.alignment = .Center
        
        universityLabel = UILabel()
        universityLabel.numberOfLines = 0
        universityLabel.textColor = UIColor.grayColor()
        universityLabel.textAlignment = .Center
        universityLabel.backgroundColor = UIColor.whiteColor()
        
        let attributedString = NSMutableAttributedString(string: "\n Think of you \n ")
        attributedString.setAttributes([NSFontAttributeName : UIFont.leagueScriptFontWithSize(27),
            NSParagraphStyleAttributeName : paragraphStyle],
            range: NSMakeRange(0, attributedString.length))
        
        universityLabel.attributedText = attributedString
        view.addSubview(universityLabel)
        
        infoTextLabel = UILabel()
        infoTextLabel.textColor = UIColor.whiteColor()
        infoTextLabel.textAlignment = .Center
        infoTextLabel.backgroundColor = bgColor_ENABLED
        infoTextLabel.text = "\n 'Think of you' wurde im Rahmen der LVA Exploring Disruptive Technologies unter der Leitung von Prof. Purgathofer umgesetzt. \n"
        infoTextLabel.numberOfLines = 0
        infoTextLabel.font = UIFont(name: "HelveticaNeue", size: 18)
        view.addSubview(infoTextLabel)
        
        lvaLabel = UILabel()
        lvaLabel.textColor = UIColor.whiteColor()
        lvaLabel.textAlignment = .Center
        lvaLabel.backgroundColor = bgColor_ENABLED
        lvaLabel.text = "\n Exploring Disruptive Technologies, \n 187.A83, VU, 2015S \n Technical University of Vienna \n"
        lvaLabel.numberOfLines = 0
        lvaLabel.font = UIFont(name: "HelveticaNeue", size: 18)
        view.addSubview(lvaLabel)

        smallLabel = UILabel()
        smallLabel.text = "\n (c) Larissa Kalbhenn \n"
        smallLabel.numberOfLines = 0
        smallLabel.font = UIFont(name: "HelveticaNeue", size: 9)
        smallLabel.textColor = UIColor.grayColor()
        smallLabel.backgroundColor = UIColor.whiteColor()
        smallLabel.textAlignment = .Center
        view.addSubview(smallLabel)
        
        universityLabel.mas_makeConstraints { maker in
            maker.left.equalTo()(self.view.mas_left).with().offset()(0)
            maker.right.equalTo()(self.view.mas_right).with().offset()(0)
            maker.top.equalTo()(self.view.mas_top).with().offset()(navHight!+40)
            maker.height.equalTo()(85)
            return
        }
        
        infoTextLabel.mas_makeConstraints { maker in
            maker.left.equalTo()(self.view.mas_left).with().offset()(0)
            maker.right.equalTo()(self.view.mas_right).with().offset()(0)
            maker.top.equalTo()(self.universityLabel.mas_bottom).with().offset()(40)
//            maker.height.equalTo()(220)
            return
        }
        
        lvaLabel.mas_makeConstraints { maker in
            maker.left.equalTo()(self.view.mas_left).with().offset()(0)
            maker.right.equalTo()(self.view.mas_right).with().offset()(0)
            maker.top.equalTo()(self.infoTextLabel.mas_bottom).with().offset()(40)
            //            maker.height.equalTo()(220)
            return
        }
        
        smallLabel.mas_makeConstraints { maker in
            maker.left.equalTo()(self.view.mas_left).with().offset()(20)
            maker.right.equalTo()(self.view.mas_right).with().offset()(-10)
            maker.bottom.equalTo()(self.view.mas_bottom).with().offset()(0)
            return
        }
    }
    
    override func viewWillAppear(animated: Bool) {

    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func okAction(){
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    
}



