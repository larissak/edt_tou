//
//  TOUDeviceDetailViewController.swift
//  ThinkOfU
//
//  Created by Larissa Kalbhenn on 09.05.15.
//  Copyright (c) 2015 com.tailored-apps. All rights reserved.
//

import Foundation
import UIKit


class TOUDeviceDetailViewController: TOUBaseDeviceDetailViewController {
    
    
    var deviceName: UILabel!
    var deviceIdentifier: UILabel!
    var bleDeviceObject : BLEDeviceObject!
    var missUButton : UIButton!
    var heyButton : UIButton!
    var thinkOfUButton : UIButton!
    var infoLabel : UILabel!
    
    var devices : NSMutableArray!
    var devicesNames : NSMutableArray!
    var devicesRssi : NSMutableArray!
    
    var bgColor_ENABLED : UIColor!
    var bgColor_DISABLED : UIColor!
    var bgColor_PINK : UIColor!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //UIColor.fromHex(0xBABABA) pink
        bgColor_ENABLED = UIColor.fromHex(0x87CFD8)
        bgColor_DISABLED = UIColor.fromHexAlpha(0x87CFD8)
        bgColor_PINK = UIColor.fromHexAlpha(0xFF5C60)
        
        view.backgroundColor = UIColor.whiteColor()
        navigationController?.navigationBar.hidden = false
        navigationController?.navigationBar.translucent = true
        navigationController?.navigationBar.tintColor = bgColor_ENABLED
        
        let navHight = navigationController?.navigationBar.frame.size.height

        deviceName = UILabel()
        deviceName.textColor = UIColor.whiteColor()
        deviceName.textAlignment = .Center
        deviceName.backgroundColor = bgColor_ENABLED
        deviceName.text = bleDeviceObject.nameOfDevice
        deviceName.numberOfLines = 0
        deviceName.font = UIFont(name: "HelveticaNeue", size: 30)
        view.addSubview(deviceName)
        
        deviceIdentifier = UILabel()
        deviceIdentifier.text = "Device ID: \(bleDeviceObject.deviceID)"
        deviceIdentifier.numberOfLines = 0
        deviceIdentifier.font = UIFont(name: "HelveticaNeue", size: 9)
        deviceIdentifier.textColor = UIColor.clearColor()
        view.addSubview(deviceIdentifier)
        
        infoLabel = UILabel()
        infoLabel.numberOfLines = 0
        infoLabel.textColor = UIColor.grayColor()
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 15
        paragraphStyle.alignment = .Center
        
        let attributedString = NSMutableAttributedString(string: "\n Say Hi to \(bleDeviceObject.nameOfDevice)  \n")
        attributedString.setAttributes([NSFontAttributeName : UIFont.leagueScriptFontWithSize(19),
            NSParagraphStyleAttributeName : paragraphStyle],
            range: NSMakeRange(0, attributedString.length))
        
        infoLabel.attributedText = attributedString
        view.addSubview(infoLabel)
        
        missUButton = UIButton()
        missUButton.addTarget(self, action: Selector("missUAction"), forControlEvents: .TouchUpInside)
        missUButton.setTitle("I miss you", forState: .Normal)
        missUButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        missUButton.backgroundColor = bgColor_ENABLED
        missUButton.tag = 3
        
        heyButton = UIButton()
        heyButton.setTitle("Just want to say HEY :)", forState: .Normal)
        heyButton.addTarget(self, action: Selector("heyAction"), forControlEvents: .TouchUpInside)
        heyButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        heyButton.backgroundColor = bgColor_ENABLED
        heyButton.tag = 1
        
        thinkOfUButton = UIButton()
        thinkOfUButton.setTitle("Think of you", forState: .Normal)
        thinkOfUButton.addTarget(self, action: Selector("touAction"), forControlEvents: .TouchUpInside)
        thinkOfUButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        thinkOfUButton.backgroundColor = bgColor_ENABLED
        thinkOfUButton.tag = 2
        
        view.addSubview(missUButton)
        view.addSubview(heyButton)
        view.addSubview(thinkOfUButton)
        
        deviceName.mas_makeConstraints { maker in
            maker.left.equalTo()(self.view.mas_left).with().offset()(0)
            maker.right.equalTo()(self.view.mas_right).with().offset()(0)
            maker.top.equalTo()(self.view.mas_top).with().offset()(navHight!+20)
            maker.height.equalTo()(80)
            return
        }
        
        deviceIdentifier.mas_makeConstraints { maker in
            maker.left.equalTo()(self.view.mas_left).with().offset()(10)
            maker.right.equalTo()(self.view.mas_right).with().offset()(-10)
            maker.top.equalTo()(self.deviceName.mas_bottom).with().offset()(10)
            return
        }
        
        infoLabel.mas_makeConstraints { maker in
            maker.left.equalTo()(self.view.mas_left).with().offset()(10)
            maker.right.equalTo()(self.view.mas_right).with().offset()(-10)
            maker.top.equalTo()(self.deviceIdentifier.mas_bottom).with().offset()(8)
            return
        }
        
        missUButton.mas_makeConstraints { maker in
            maker.left.equalTo()(self.view.mas_left).with().offset()(0)
            maker.right.equalTo()(self.view.mas_right).with().offset()(0)
            maker.bottom.equalTo()(self.view.mas_bottom).with().offset()(-10)
            maker.height.equalTo()(80)
            return
        }
        
        thinkOfUButton.mas_makeConstraints { maker in
            maker.left.equalTo()(self.view.mas_left).with().offset()(0)
            maker.right.equalTo()(self.view.mas_right).with().offset()(0)
            maker.bottom.equalTo()(self.missUButton.mas_top).with().offset()(-20)
            maker.height.equalTo()(80)
            return
        }
        
        heyButton.mas_makeConstraints { maker in
            maker.left.equalTo()(self.view.mas_left).with().offset()(0)
            maker.right.equalTo()(self.view.mas_right).with().offset()(0)
            maker.bottom.equalTo()(self.thinkOfUButton.mas_top).with().offset()(-20)
            maker.height.equalTo()(80)
            return
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        seas()
        interfaceUpdate_ENABLED()
        connectToPeriphal()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func seas() {
        super.seas()
    }
    
    //MARK: custom Button Actions
    
    func showAlertMSGwithTitle(title: String, msg: String){
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.Alert)
        alert.view.tintColor = bgColor_ENABLED
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    override func missUAction(){
        super.missUAction()
        print("missUAction")
        disableUserInteraction(missUButton.tag)
//        showAlertMSGwithTitle("I miss you", msg: "Message sent.")
    }
    
    override func touAction(){
        print("touAction")
        super.touAction()
        disableUserInteraction(thinkOfUButton.tag)
//        showAlertMSGwithTitle("Think of you", msg: "Message sent.")
    }
    
    override func heyAction(){
        print("heyAction")
        super.heyAction()
        disableUserInteraction(heyButton.tag)
//        showAlertMSGwithTitle("Just want to say HEY :)", msg: "Message sent.")
    }
    
    func disableUserInteraction(buttonTag : Int){

        interfaceUpdate_DISABLED(buttonTag)
        
        _ = NSTimer.scheduledTimerWithTimeInterval(5.0, target: self, selector: Selector("enableUserInteraction"), userInfo: nil, repeats: false)
    }
    
    func enableUserInteraction(){
        
        interfaceUpdate_ENABLED()
//        showAlertMSGwithTitle("Message sent.", msg: "\(bleDeviceObject.nameOfDevice) will jump for joy.")
//        SVProgressHUD.showSuccessWithStatus("Message sent. :)")
        SVProgressHUD.showSuccessWithStatus("Message sent.", maskType: .Gradient)
    }
    
    func interfaceUpdate_DISABLED(buttonTag : Int?){
        
        /*
            heyButton: tag 1
            thinkOfUButton: tag 2
            missUButton: tag 3
        */
        
        missUButton.enabled = false
        thinkOfUButton.enabled = false
        heyButton.enabled = false
        
        missUButton.backgroundColor = bgColor_DISABLED
        thinkOfUButton.backgroundColor = bgColor_DISABLED
        heyButton.backgroundColor = bgColor_DISABLED
        
        if let bTag = buttonTag{
            if bTag == 1{
                heyButton.backgroundColor = bgColor_PINK
            }else if bTag == 2{
                thinkOfUButton.backgroundColor = bgColor_PINK
            }else if bTag == 3{
                missUButton.backgroundColor = bgColor_PINK
            }
        }
    }
    
    func interfaceUpdate_ENABLED(){
        missUButton.enabled = true
        thinkOfUButton.enabled = true
        heyButton.enabled = true
        
        missUButton.backgroundColor = bgColor_ENABLED
        thinkOfUButton.backgroundColor = bgColor_ENABLED
        heyButton.backgroundColor = bgColor_ENABLED
    }
    
    override func showErrorMSG() {
        showAlertMSGwithTitle("Ups...", msg: "Es ist leider ein Fehler aufgetreten. Bitte verbinde dich nochmal.")
        
        interfaceUpdate_DISABLED(nil)
    }
    
    func connectToPeriphal(){
        
//        self.ble.connectPeripheral(ble.peripherals[0] as! CBPeripheral)
        
        if let bluetoothLE = self.ble{
            bluetoothLE.connectPeripheral(ble.peripherals[0] as! CBPeripheral)
        }
    }
    
    func findPINS(){
        
    }
    
}



