//
//  TOUBaseConnectionViewController.m
//  ThinkOfU
//
//  Created by Larissa Kalbhenn on 22.05.15.
//  Copyright (c) 2015 com.tailored-apps. All rights reserved.
//

#import "TOUBaseDeviceDetailViewController.h"


uint8_t total_pin_count  = 0;
uint8_t pin_mode[128]    = {0};
uint8_t pin_cap[128]     = {0};
uint8_t pin_digital[128] = {0};
uint16_t pin_analog[128]  = {0};
uint8_t pin_pwm[128]     = {0};
uint8_t pin_servo[128]   = {0};

uint8_t init_done = 0;

@interface TOUBaseDeviceDetailViewController ()<ProtocolDelegate>

@end

@implementation TOUBaseDeviceDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.protocol = [[RBLProtocol alloc] init];
    self.protocol.delegate = self;
    self.protocol.ble = self.ble;
    

    NSLog(@"... -> TOUBaseDeviceDetailViewController: viewDidLoad");
}

-(void)viewDidAppear:(BOOL)animated{
    NSLog(@"... -> TOUBaseDeviceDetailViewController: viewDidAppear");

    uint8_t pin = 9;
    current_pin = pin;
    [self.protocol setPinMode:current_pin Mode:OUTPUT];
    
    self.counter = 0;
    [self.protocol queryProtocolVersion];
}

-(void)viewDidDisappear:(BOOL)animated
{
    NSLog(@".. -> TOUBaseDeviceDetailViewController: viewDidDisappear");
    
    total_pin_count = 0;
    init_done = 0;
}


-(void)seas{
    NSLog(@"super.seeas()");
}

-(void)showNotConnectedMSG{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Fehler" message:@"Nicht Verbunden." preferredStyle:UIAlertControllerStyleAlert];
    [alert.view setTintColor:[UIColor colorWithRed:135.0 green:207.0 blue:216.0 alpha:1]];
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:ok];
    
    [self presentViewController:alert animated:YES completion:nil];
}


#pragma mark
#pragma mark Button Actions

uint8_t current_pin = 0;


/*
 MISS
 
 'L','N','N'
 */
-(void)missUAction{
    NSLog(@"super.missUAction()");
    
    if (self.ble.isConnected) {
        uint8_t pin = 9;
        current_pin = pin;
        
        [self.protocol MISS_digitalWrite:pin Value:HIGH];
        
    }else{
        NSLog(@"BLE NOT CONNECTED");
        [self showNotConnectedMSG];
    }
}



/*
 TOU
 
 'K','L','K'
 */
-(void)touAction{
    NSLog(@"super.touAction()");

    if (self.ble.isConnected) {
        uint8_t pin = 9;
        current_pin = pin;
        
        [self.protocol TOU_digitalWrite:pin Value:HIGH];

    }else{
        NSLog(@"BLE NOT CONNECTED");
        [self showNotConnectedMSG];
    }
}


/*
 HEY
 
 'K','K','K'
 */
-(void)heyAction{
    NSLog(@"super.heyAction()");
    
    if (self.ble.isConnected) {
        
        uint8_t pin = 9;
        current_pin = pin;
        
        [self.protocol HEY_digitalWrite:pin Value:HIGH];
//        pin_digital[pin] = HIGH;
        
//        [NSTimer scheduledTimerWithTimeInterval:3.0 target:self selector:@selector(changeHeyHL) userInfo:nil repeats:NO];
        
    }else{
        NSLog(@"BLE NOT CONNECTED");
        [self showNotConnectedMSG];
    }
}



#pragma mark
#pragma mark Process DATA

-(void) processData:(uint8_t *) data length:(uint8_t) length{
    
    [self.protocol parseData:data length:length];
}





#pragma mark
#pragma mark Protocol Delegate Methods

-(void) protocolDidReceiveProtocolVersion:(uint8_t)major Minor:(uint8_t)minor Bugfix:(uint8_t)bugfix
{
    NSLog(@"protocolDidReceiveProtocolVersion: %d.%d.%d", major, minor, bugfix);
    
    [SVProgressHUD showWithStatus:@"Lade Daten..." maskType:SVProgressHUDMaskTypeGradient];
    
    uint8_t buf[] = {'B', 'L', 'E'};
    [self.protocol sendCustomData:buf Length:3];
    [self.protocol queryTotalPinCount];
}

-(void) protocolDidReceiveTotalPinCount:(UInt8) count
{
    NSLog(@"protocolDidReceiveTotalPinCount: %d", count);
    
    total_pin_count = count;
//    [self.protocol queryPinAll];
    
    if ([SVProgressHUD isVisible]) {
        [SVProgressHUD dismiss];
    }
}

-(void) protocolDidReceivePinCapability:(uint8_t)pin Value:(uint8_t)value
{
    NSLog(@"protocolDidReceivePinCapability");
    NSLog(@" Pin %d Capability: 0x%02X", pin, value);
    
    self.counter++;
    
    if(self.counter == 23){
        if ([SVProgressHUD isVisible]) {
            [SVProgressHUD dismiss];
        }
    }
    
//    if (value == 0)
//        NSLog(@" - Nothing");
//    else
//    {
//        if (value & PIN_CAPABILITY_DIGITAL)
//            NSLog(@" - DIGITAL (I/O)");
//        if (value & PIN_CAPABILITY_ANALOG)
//            NSLog(@" - ANALOG");
//        if (value & PIN_CAPABILITY_PWM)
//            NSLog(@" - PWM");
//        if (value & PIN_CAPABILITY_SERVO)
//            NSLog(@" - SERVO");
//    }
//    
//    pin_cap[pin] = value;
}

-(void) protocolDidReceivePinData:(uint8_t)pin Mode:(uint8_t)mode Value:(uint8_t)value
{
    //    NSLog(@"protocolDidReceiveDigitalData");
    //    NSLog(@" Pin: %d, mode: %d, value: %d", pin, mode, value);
    
    uint8_t _mode = mode & 0x0F;
    
    pin_mode[pin] = _mode;
    if ((_mode == INPUT) || (_mode == OUTPUT))
        pin_digital[pin] = value;
    else if (_mode == ANALOG)
        pin_analog[pin] = ((mode >> 4) << 8) + value;
    else if (_mode == PWM)
        pin_pwm[pin] = value;
    else if (_mode == SERVO)
        pin_servo[pin] = value;
}

-(void) protocolDidReceivePinMode:(uint8_t)pin Mode:(uint8_t)mode
{
    NSLog(@"protocolDidReceivePinMode");
    
    if (mode == INPUT)
        NSLog(@" Pin %d Mode: INPUT", pin);
    else if (mode == OUTPUT)
        NSLog(@" Pin %d Mode: OUTPUT", pin);
    else if (mode == PWM)
        NSLog(@" Pin %d Mode: PWM", pin);
    else if (mode == SERVO)
        NSLog(@" Pin %d Mode: SERVO", pin);
    
    pin_mode[pin] = mode;
}

-(void) protocolDidReceiveCustomData:(UInt8 *)data length:(UInt8)length
{
    // Handle your customer data here.
    for (int i = 0; i< length; i++)
        printf("0x%2X ", data[i]);
    printf("\n");
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
