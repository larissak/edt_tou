//
//  TOUConnectionViewController.swift
//  ThinkOfU
//
//  Created by Larissa Kalbhenn on 09.05.15.
//  Copyright (c) 2015 com.tailored-apps. All rights reserved.
//

import Foundation
import UIKit


class DeviceTableViewCell : UITableViewCell {
    
    var deviceTextField : UITextField!
    var deviceImageView : UIImageView!
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        deviceImageView = UIImageView(image: UIImage(named: "img_heart_semitrans"))
        
        deviceTextField = UITextField()
        deviceTextField.borderStyle = .None
        deviceTextField.textColor = UIColor.fromHex(0xBABABA)
        deviceTextField.userInteractionEnabled = false
        deviceTextField.enabled = false
        deviceTextField.placeholder = "-"
        deviceTextField.leftView = deviceImageView
        deviceTextField.leftViewMode = .Always
        deviceTextField.font = UIFont(name: "HelveticaNeue", size: 18)
        deviceTextField.textAlignment = .Center
        
        contentView.addSubview(deviceTextField)
        
        deviceTextField.mas_makeConstraints { maker in
            maker.edges.equalTo()(self.contentView).with().insets()(UIEdgeInsets(top: 2, left: 10, bottom: 7, right: 10))
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


class TOUConnectionViewController: UIViewController , BLEDelegate,UITableViewDelegate, UITableViewDataSource{
    
    @IBOutlet var infoButton: UIButton!
    @IBOutlet var tableViewBackgroundView: UIView!
    @IBOutlet var appTitle: UILabel!
    @IBOutlet var logoImageView: UIImageView!
    @IBOutlet var searchDeviceButton: UIButton!
    @IBOutlet var loadingIndicator: UIActivityIndicatorView!
    var ble : BLE!
    var devices : NSMutableArray!
    var devicesNames : NSMutableArray!
    var devicesRssi : NSMutableArray!
    var tableView : UITableView!
    var deviceDetailVC : TOUDeviceDetailViewController!
    var touForegroundColor : UIColor!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.hidden = true
        
        devices = NSMutableArray()
        devicesNames = NSMutableArray()
        devicesRssi = NSMutableArray()
        
        loadingIndicator.hidesWhenStopped = true
        
        touForegroundColor = UIColor.fromHex(0xFF5C60)
        
        ble = BLE()
        ble.delegate = self
        ble.controlSetup()
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 15
        paragraphStyle.alignment = .Center
        
        let attributedString = NSMutableAttributedString(string: "Think of U")
        attributedString.setAttributes([NSFontAttributeName : UIFont.leagueScriptFontWithSize(30),
            NSParagraphStyleAttributeName : paragraphStyle],
            range: NSMakeRange(0, attributedString.length))
        
        appTitle.attributedText = attributedString
        
        infoButton.setTitleColor(touForegroundColor, forState: .Normal)
        infoButton.layer.borderWidth = 1.0
        infoButton.layer.borderColor = touForegroundColor.CGColor
        
        searchDeviceButton.backgroundColor = UIColor.whiteColor()
        searchDeviceButton.tintColor = touForegroundColor
        searchDeviceButton.setTitleColor(touForegroundColor, forState: .Normal)
        searchDeviceButton.layer.borderWidth = 1.0
        searchDeviceButton.layer.borderColor = touForegroundColor.CGColor
        
        tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = UIColor.whiteColor()
        tableView.registerClass(DeviceTableViewCell.self, forCellReuseIdentifier: NSStringFromClass(DeviceTableViewCell.self))
        tableView.separatorStyle = .None
        tableView.rowHeight = 60
        tableView.layer.borderWidth = 1.0
        tableView.layer.borderColor = UIColor.fromHex(0xFF5C60).CGColor
        
        tableViewBackgroundView = UIView()
        tableViewBackgroundView.backgroundColor = UIColor.redColor()
        tableViewBackgroundView.addSubview(tableView)
        view.addSubview(tableViewBackgroundView)
        
        tableViewBackgroundView.mas_makeConstraints { maker in
            maker.left.equalTo()(self.view.mas_left).with().offset()(20)
            maker.right.equalTo()(self.view.mas_right).with().offset()(-20)
            maker.height.equalTo()(60)
            maker.bottom.equalTo()(self.view.mas_bottom).with().offset()(-40)
            return
        }
        
        tableView.mas_makeConstraints { maker in
            maker.edges.equalTo()(self.tableViewBackgroundView).with().insets()(UIEdgeInsetsZero)
            return
        }
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
         navigationController?.navigationBar.hidden = true
        searchDeviceButton.enabled = true
        
        devices.removeAllObjects()
        devicesNames.removeAllObjects()
        devicesRssi.removeAllObjects()
        checkConnection()
        
        tableView.reloadData()
    }
    
    func checkConnection(){
        if let activePeriphal = ble.activePeripheral{
            if activePeriphal.state == .Connected{
                ble.CM.cancelPeripheralConnection(ble.activePeripheral)
                return
            }
        }
        
        if let periphals = ble.peripherals{
            ble.peripherals = nil
        }
    }
    
    @IBAction func infoButtonAction(sender: AnyObject) {
        
        let infoVC = TOUInfoViewController()
        navigationController?.presentViewController(UINavigationController(rootViewController: infoVC), animated: true, completion: nil)
    }
    
    @IBAction func searchDeviceButtonAction(sender: AnyObject) {
        print("searchDeviceButtonAction")
        
        loadingIndicator.startAnimating()
        checkConnection()
        searchDeviceButton.enabled = false
        ble.findBLEPeripherals(3)
        
        _ = NSTimer.scheduledTimerWithTimeInterval(3.0, target: self, selector: Selector("connectionTimer:"), userInfo: nil, repeats: false)
    }
    
    
    func connectionTimer(timer : NSTimer){
        loadingIndicator.stopAnimating()
        searchDeviceButton.enabled = true
        
        if let periphals = ble.peripherals{
            if ble.peripherals.count > 0{
                
                devices.removeAllObjects()
                devicesNames.removeAllObjects()
                devicesRssi.removeAllObjects()
                
                let peripheralsArray = ble.peripherals as NSArray as! [CBPeripheral]
                
                for cbPeripheral : CBPeripheral in peripheralsArray {
                    
//                    if let uuidIdentifier = cbPeripheral.identifier{
//                        devices.addObject(uuidIdentifier)
//                    }
                    let uuidIdentifier = cbPeripheral.identifier
                    devices.addObject(uuidIdentifier)

                    
                    if let pName = cbPeripheral.name{
                        devicesNames.addObject(pName)
                    }
                }
                
                tableView.reloadData()
            }
            else{
                showAlertMSGwithTitle("No Devices", msg: "No devices found.")
            }

        }else{
            showAlertMSGwithTitle("No Devices", msg: "No devices found.")
        }
    }
    
    func showAlertMSGwithTitle(title: String, msg: String){
        let alert = UIAlertController(title: nil, message: msg, preferredStyle: UIAlertControllerStyle.Alert)
        alert.view.tintColor = touForegroundColor
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    //MARK: BLE Delegate Methods
    func bleDidConnect() {
        print("... -> ble did connect")
    }
    
    func bleDidDisconnect() {
        print("... -> ble did disconnect")
        
        let vc = navigationController?.topViewController

        if (vc!.isKindOfClass(TOUDeviceDetailViewController.self)){
            if deviceDetailVC != nil{
                deviceDetailVC.showErrorMSG()
            }
        }
    }
    
    /*
        called when a data is sent from BLE peripheral to the App.
    */
    func bleDidReceiveData(data: UnsafeMutablePointer<UInt8> , length: Int32) {
       print("... -> ble did receive data \(data)")

        let lengthU8 = UInt8(length)
        
        if deviceDetailVC != nil{
            deviceDetailVC.processData(data, length: lengthU8)
        }
        
        //    -(void) bleDidReceiveData:(unsigned char *)data length:(int)length
        // uint8_t *crypto_data = (uint8_t*)bytes;
    }
    
    func bleDidUpdateRSSI(rssi: NSNumber!) {
        print("... -> ble DidUpdateRSSI")
    }
    
    //MARK: UITableView Delegate Methods
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if devices.count > 0{
            return devices.count
        }else{
            return 1
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        
        let cell = tableView.dequeueReusableCellWithIdentifier(NSStringFromClass(DeviceTableViewCell.self), forIndexPath: indexPath) as! DeviceTableViewCell
        cell.selectionStyle = .None
        tableView.scrollEnabled = false
        var deviceAvailable = false
        
        if devices.count > 0{
            tableView.scrollEnabled = true
            if devices.count == 0{
                tableView.scrollEnabled = false
            }
            let name = devicesNames[indexPath.row] as! String
            cell.deviceTextField.text = "\(name)"
            deviceAvailable = true
        }else{
            cell.deviceTextField.text = "No device"
            deviceAvailable = false
        }

        cell.deviceTextField.textColor = deviceAvailable == true ? UIColor.fromHex(0xFF5C60): UIColor.fromHex(0xBABABA)
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {

        if devices.count > 0{
            let newBLEObject : BLEDeviceObject = BLEDeviceObject(name: devicesNames[indexPath.row] as! String, indetifier: devices[indexPath.row].UUIDString )
            deviceDetailVC = TOUDeviceDetailViewController()
            deviceDetailVC.devices = devices
            deviceDetailVC.devicesNames = devicesNames
            deviceDetailVC.ble = ble
            deviceDetailVC.bleDeviceObject = newBLEObject
            navigationController?.pushViewController(deviceDetailVC, animated: true)
            
        }else{
            print("... no devices to show")
            
            /*
                -> comment out for testing the interface:
                no BLE connection needed to push a TOUDeviceDetailViewController -> only for interface testing!!!
            */
            
//            let newBLEObject : BLEDeviceObject = BLEDeviceObject(name: "Test Device", indetifier: "kljalfjwöejqböreqöiveböq")
//            let deviceCetailVC = TOUDeviceDetailViewController()
//            deviceCetailVC.bleDeviceObject = newBLEObject
//            navigationController?.pushViewController(deviceCetailVC, animated: true)
        }
    }
    
}



