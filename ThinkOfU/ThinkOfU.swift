//
//  ThinkOfU.swift
//  ThinkOfU
//
//  Created by Larissa Kalbhenn on 19.05.15.
//  Copyright (c) 2015 com.tailored-apps. All rights reserved.
//

import Foundation
import UIKit


let currentColor = UIColor(red:0.61, green:0.74, blue:0.69, alpha:1)


extension UIColor{
    
    class func fromHex(rgbValue: UInt) -> UIColor {
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    class func fromHexAlpha(rgbValue: UInt) -> UIColor {
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(0.5)
        )
    }
    
}


extension UIFont {
    
    class func leagueScriptFontWithSize(size : CGFloat) -> UIFont {
        return UIFont(name: "LeagueScriptThin-LeagueScript", size: size)!
    }
}


class BLEDeviceObject: NSObject {
    var nameOfDevice : String!
    var deviceID: String!
    
    init(name: String, indetifier : String) {
        self.nameOfDevice = name
        self.deviceID = indetifier
        super.init()
    }
}

extension UIView {
    
    func shakeWithDefaultValues(){
        shake(4, positionChange: CGPoint(x: 8, y: 0))
    }
    
    func shake(times: Int , positionChange: CGPoint){
        let pos = layer.position;
        let shakeAnimation = CAKeyframeAnimation(keyPath: "position")
        let shakePath = CGPathCreateMutable()
        CGPathMoveToPoint(shakePath, nil, pos.x, pos.y);
        
        for i in 0..<times{
            CGPathAddLineToPoint(shakePath,nil,pos.x-positionChange.x,pos.y-positionChange.y)
            CGPathAddLineToPoint(shakePath,nil,pos.x+positionChange.x,pos.y+positionChange.y)
        }
        CGPathAddLineToPoint(shakePath, nil, pos.x, pos.y);
        CGPathCloseSubpath(shakePath);
        shakeAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
        shakeAnimation.duration = 1.2;
        shakeAnimation.path = shakePath;
        layer.addAnimation(shakeAnimation, forKey: nil)
    }
    
    class func loadFromNib() -> UIView! {
        var name = NSStringFromClass(self.self)
        name = name.componentsSeparatedByString(".").last!
        return UINib(nibName: name,bundle: nil).instantiateWithOwner(nil, options: nil).first as! UIView
    }
}