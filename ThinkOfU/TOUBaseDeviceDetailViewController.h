//
//  TOUBaseConnectionViewController.h
//  ThinkOfU
//
//  Created by Larissa Kalbhenn on 22.05.15.
//  Copyright (c) 2015 com.tailored-apps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RBLProtocol.h"
#import "BLE.h"
#import "SVProgressHUD.h"


@interface TOUBaseDeviceDetailViewController : UIViewController

@property (strong, nonatomic) BLE *ble;
@property (strong, nonatomic) RBLProtocol *protocol;
@property int counter;

@property (nonatomic) BOOL missPresssed;
@property (nonatomic) BOOL touPresssed;
@property (nonatomic) BOOL heyPresssed;

-(void) processData:(uint8_t *) data length:(uint8_t) length;

-(void)missUAction;
-(void)touAction;
-(void)heyAction;

-(void)seas;

-(void)showErrorMSG;

@end
